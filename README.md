##= Introduction =
 
A basic bookstore. This is a project with one only purpose: learn.
 
##= Requirements =
 
All code was writen using PHP 5.4.7.

##== External Deps  ==
 
* write dependencies here
 
##= Installation =
 
Download or clone this repository into your server folder, and have fun.
   
##= Tests =
 
No unit tests were implemented.

##= More Information =
 
Ping me at Twitter (@devdrops).
 

##= Example Usage =
 
write example here
